// import {ngApp, ngModule, ngService, ngController, ngWebComponent, ngFilter} from './ng';
import angular from 'angular';
import ngNewRouter from 'angular-new-router';

class ngModule {

  get name() {
    return Object.getPrototypeOf(this).constructor.name;
  }

  get requires() {
    return [];
  }

  constructor() {
    this._unites = [];
  }

  add(unit) {
    this._unites.push(unit);
    return this;
  }

  define() {
    this.module = angular.module(this.name, this.requires);
    this._unites.forEach((unit) => unit._define(this.module));
    return this;
  }

}

class ngApp extends ngModule {

  constructor(container) {
    super();
    this.container = container;
  }

  define() {
    super.define();
    angular.bootstrap(this.container, [this.name]);
  }

}

class ngUnit {

  get requires() {
    return [];
  }

  get params() {
    return this._params || {};
  }

  get name() {
    return this._name || Object.getPrototypeOf(this).constructor.name;
  }

  constructor(params, name) {
    this._name = name;
    this._params = params;
  }

  _define(module) {}

  _inject() {
    this.requires.forEach((dep) => this[dep] = this.$injector.get(dep));
  }

  _wrap() {
    var instance = this;
    return function($injector) {
        instance.$injector = $injector;
        instance._inject();
        Object.assign(this, instance);
        Object.getOwnPropertyNames(instance.constructor.prototype).forEach(
          (method) => this[method] = instance.constructor.prototype[method]
        );
        if (this.init) this.init();
    }
  }

  init() {}

}

class ngController extends ngUnit {
  _define(module) {
    module.controller(this.name, ['$injector', this._wrap()]);
  }
}

class ngService extends ngUnit {
  _define(module) {
    module.service(this.name, ['$injector', this._wrap()]);
  }
}

class ngFilterCollection extends ngUnit {
  _define(module) {
    var instance = this;
    Object.getOwnPropertyNames(instance.constructor.prototype).forEach(
      (method) => {
        if (method != 'constructor') {
            module.filter(method, instance.constructor.prototype[method]);
        }
      }
    );
  }
}

class ngComponent extends ngUnit {
  _define(module) {
    
  }
}

// -----------------------------------------------------------------------------

class TestModule extends ngModule {
  get requires() {
    return ['ngNewRouter'];
  }
}

class App extends ngApp {
  get requires() {
    return ['TestModule'];
  }
}

class AppController extends ngController {
  get requires() {
    return ['$q', 'OrderRestService'];
  }
  init() {
    this.title = '123';
    let d = this.$q.defer();
    d.promise.then(this.test.bind(this));
    setTimeout(function() {
      d.resolve({});
    }, 500)
  }
  test() {
    this.title = 'TEST';
    this.OrderRestService.log('Hello from controller method and injected service');
  }
}

class TestService extends ngService {
  get requires() {
    return ['$http'];
  }
  log(data) {
    console.log(this._params.url);
    console.log(data);
  }
}

class TestFilters extends ngFilterCollection {
  lower($filter) {
    return function(text) {
      return $filter('lowercase')(text);
    }
  }
  upper() {
    return function(text) {
      return text.toUpperCase();
    }
  }
}

new TestModule()
  .add(new AppController())
  .add(new TestService({
    url: '/api'
  }, 'OrderRestService'))
  .add(new TestFilters())
  .define();

new App(document.body)
  .define();
