import datetimepicker from 'jquery-datetimepicker';
import datetimepickerStyle from 'jquery-datetimepicker/jquery.datetimepicker.css'

export var dateTimePickerDecorator = function(node, options) {  
  $(node).datetimepicker(options);
  return {
    update: function() {
      debugger
    },
    teardown: function() {
      $(node).datetimepicker('destroy');
    }
  }
}
