import t from './index.html';
import {list} from './components/list/listComponent.js';
import {dateTimePickerDecorator} from './decorators/datetimepicker/dateTimePickerDecorator.js';

var ractive = new Ractive({
  el: '#container',
  template: '#app-view',
  data: {
    title: 'Hello!',
    items: [
      {value: '1'},
      {value: '2'},
      {value: '3'},
      {value: '4'}
    ]
  },
  components: {list},
  decorators: {datetimepicker: dateTimePickerDecorator},
  oninit: function() {
    this.findComponent('list').on('item-selected', (ev, item) => this.set('date', item.value));    
  },
  add: function() {
    let items = this.get('items');
    items.push({value: this.get('date')});
  }
});
