var HtmlWebpackPlugin = require('html-webpack-plugin'),
  webpack = require('webpack');
var config = {
    entry: [
      './app.js',
    ],
    addVendor: function (name, path) {
       this.resolve.alias[name] = path;
       this.module.noParse.push(new RegExp(path));
   },
    // resolve: {
    //   alias: {
    //     jquery: node_dir + '/jquery/dist/jquery.js',
    //     jquery.elastic: lib_dir + '/jquery.elastic.source.js'
    //   }
    // },
    output: {
        path: __dirname + '/public',
        filename: 'bundle.js'
    },
    module: {
      preLoaders: [],
      loaders: [{
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /.mustache$/,
        loader: 'raw'
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot|html)$/,
        loader: 'file'
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less-loader'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
       $: "jquery",
       jQuery: "jquery",
       Ractive: "ractive"
   })
 ],
  devtool: 'sourcemap'
};


module.exports = config;
