import Ractive from 'ractive';
import view from './listView.mustache';
import style from './listStyle.less';

export var list = Ractive.extend({
  data: function() {
    return {
      items: []
    }
  },
  template: view,
  isolated: false,
  oninit: function() {
    console.log('ListComponent init');
  },
  select: function(item) {
  //  this.fire('item-selected', item);
  }
});

// Ractive.extend(new ListComponent);
