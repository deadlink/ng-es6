import Ractive from 'ractive';
import jquery from 'jquery';
import datetimepicker from 'jquery-datetimepicker';
import datetimepickerStyle from 'jquery-datetimepicker/jquery.datetimepicker.css'
import view from './datetimepickerView.mustache';
import style from './datetimepickerStyle.less';

export var datetimepicker = Ractive.extend({
  template: view,
  data: {

  }
});
 
